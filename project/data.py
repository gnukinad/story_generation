import os
from nltk import word_tokenize

import numpy as np

import re

import torch


from collections import Counter
from itertools import chain

device = torch.device("cuda" if torch.cuda.is_available else "cpu")


class Vocab:
    def __init__(self, counter, sos, eos, pad, unk, min_freq=None):
        self.sos = sos
        self.eos = eos
        self.pad = pad
        self.unk = unk

        self.pad_idx = 0
        self.unk_idx = 1
        self.sos_idx = 2
        self.eos_idx = 3

        self._token2idx = {
            self.sos: self.sos_idx,
            self.eos: self.eos_idx,
            self.pad: self.pad_idx,
            self.unk: self.unk_idx,
        }
        self._idx2token = {idx:token for token, idx in self._token2idx.items()}

        idx = len(self._token2idx)
        min_freq = 0 if min_freq is None else min_freq

        for token, count in counter.items():
            if count > min_freq:
                self._token2idx[token] = idx
                self._idx2token[idx]   = token
                idx += 1

        self.vocab_size = len(self._token2idx)
        self.tokens     = list(self._token2idx.keys())

    def token2idx(self, token):
        return self._token2idx.get(token, self.unk_idx)

    def idx2token(self, idx):
        return self._idx2token.get(idx, self.unk)


    def token_list2idx(self, tokens):
        return [self.token2idx(x) for x in tokens]


    def idx_list2token(self, idxs):
        return [self.idx2token(idx) for x in idxs]


    def __len__(self):
        return len(self._token2idx)



class WritingPromptsDataset:
    def __init__(self, source_filename, target_filename, device=None):

        if device is None:
            self.device = torch.device("cuda" if torch.cuda.is_available else "cpu")

        self.source_filename = source_filename
        self.target_filename = target_filename

        # print('generating stories_words, prompts_words')
        self.stories_words, self.prompts_words = self.generate_lists()

        # print('extracting tags and prompts')
        tags_prompts = [self.extract_tags_prompt(x) for x in self.prompts_words]

        # print('extracting tags and prompts separately')
        self.tags = [x[0] for x in tags_prompts]
        self.tagless_prompts = [x[1] for x in tags_prompts]

        # print('creating vocabs')


        self.sos = '<sos>'
        self.eos = '<eos>'
        self.pad = '<pad>'
        self.unk = '<unk>'

        self.min_freq = 2


        self.vocab = self.generate_vocab(self.tagless_prompts, self.stories_words)

        self.prompts_idx = [self.vocab.token_list2idx(x) for x in self.prompts_words]
        self.stories_idx = [self.vocab.token_list2idx(x) for x in self.stories_words]


        self.ntotal = len(self.prompts_idx)

        self.ratio = 0.1

        self.train_val_split(self.ratio)


    def train_val_split(self, ratio):
        '''Split dataset to train and validation datasets
        '''

        val = int(np.floor(ratio * self.ntotal))

        choice = np.random.choice(self.ntotal, self.ntotal, replace=False)


        self.train_prompts = [self.prompts_idx[i] for i in choice[:val]]
        self.train_stories = [self.stories_idx[i] for i in choice[:val]]

        self.val_prompts = [self.prompts_idx[i] for i in choice[val:]]
        self.val_stories = [self.stories_idx[i] for i in choice[val:]]


    def generate_lists(self, source_filename=None, target_filename=None):
        '''generate lists of prompts and stories after preprocessing
        '''

        if source_filename is None:
            source_filename = self.source_filename

        if target_filename is None:
            target_filename = self.target_filename

        with open(source_filename) as f:
            slines = f.readlines()

        with open(target_filename) as f:
            tlines = f.readlines()


        # apply here data cleaning
        tlines = [self.replace_new_lines(x.lower()) for x in tlines]
        slines = [self.replace_new_lines(x.lower()) for x in slines]
        # tlines = [replace_dashes(x) for x in tlines]

        all_stories = tlines[:5000]
        all_prompts = slines[:5000]

        stories_words = [word_tokenize(x.lower()) for x in all_stories]
        prompts_words = [word_tokenize(x.lower()) for x in all_prompts]

        return stories_words, prompts_words


    def generate_vocab(self, prompts_words, stories_words):
        '''def generate vocabulary
        '''

        eos = self.eos
        sos = self.sos
        pad = self.pad
        unk = self.unk

        min_freq = self.min_freq

        words_list = list(chain.from_iterable(stories_words))
        prompts_list = list(chain.from_iterable(prompts_words))


        stories_count = Counter(words_list)
        prompts_count = Counter(prompts_list)

        # stories_vocab = Vocab(stories_count, sos, eos, pad, unk, min_freq)
        # stories_idx = [stories_vocab.token_list2idx(x) for x in stories_words]

        # prompts_vocab = Vocab(prompts_count, sos, eos, pad, unk, min_freq)
        # prompts_idx = [prompts_vocab.token_list2idx(x) for x in prompts_words]

        common_counter = stories_count + prompts_count
        common_vocab = Vocab(common_counter, sos, eos, pad, unk, min_freq)

        return common_vocab


    @staticmethod
    def append_elem(x, maxlen, elem):
        return x + [elem for _ in range(maxlen - len(x))]


    @staticmethod
    def _replace_text(line, to_replace, new_text):
        '''replace "to_replace" text with "new_text"
        '''
        line = line.replace(to_replace, new_text)
        return line


    @staticmethod
    def replace_new_lines(line):
        line = line.replace('<newline>', '\n')
        return line


    @staticmethod
    def replace_dashes(line):
        regex = r'-{1}'
        reg = re.compile(regex)
        return reg.sub(line, ' - ')


    @staticmethod
    def extract_tags_prompt(line):
        op = '['
        cp = ']'

        ocount = line.count(op)
        ccount = line.count(cp)

        mincount = min(ocount, ccount)

        # print('mincount is ', mincount)

        # indexes
        oinds = [i for i, x in enumerate(line) if x == op]
        cinds = [i for i, x in enumerate(line) if x == cp]

        tags = []

        for i in reversed(range(mincount)):
            # print('i is ', i)
            tag = line[oinds[i] : cinds[i]+1]
            # print('tag is ', tag)

            # print('initial line is ', line)
            del line[oinds[i] : cinds[i]+1]
            # print('initial line is ', line)
            tags.append(tag)


        return tags, line


    def __len__(self):
        return len(self.stories_words)


    def get_batch(self, batch_size, val=False, device=None):

        '''Return the batch
        '''

        if device is None:
            device = self.device

        if val == True:
            prompts, stories = self.val_prompts, self.val_stories
        elif val == False:
            prompts, stories = self.train_prompts, self.train_stories


        n = len(prompts)

        choice = np.random.choice(n, batch_size, replace=False)

        batch_source = [prompts[i] for i in choice]
        batch_target = [stories[i] for i in choice]

        batch_target_in  = [[self.vocab.sos_idx] + x for x in batch_target]
        batch_target_out = [x + [self.vocab.eos_idx] for x in batch_target]

        nmax_source = max([len(x) for x in batch_source])
        nmax_target = max([len(x) for x in batch_target_in])

        batch_source     = torch.LongTensor([self.append_elem(x, nmax_source, self.vocab.pad_idx) for x in batch_source]).to(device)
        batch_target_in  = torch.LongTensor([self.append_elem(x, nmax_target, self.vocab.pad_idx) for x in batch_target_in]).to(device)
        batch_target_out = torch.LongTensor([self.append_elem(x, nmax_target, self.vocab.pad_idx) for x in batch_target_out]).to(device)

        return batch_source, batch_target_in, batch_target_out


if __name__ == "__main__":

    dname = os.path.join('data/writingPrompts')

    fname_source = 'train.wp_source'
    fname_target = 'train.wp_target'

    source_filename = os.path.join(dname, fname_source)
    target_filename = os.path.join(dname, fname_target)


    a = WritingPromptsDataset(source_filename, target_filename)

    batch_prompts, batch_stories_in, batch_stories_out = a.get_batch(32)
